# tsed-restaurant

> A project to manage food orders and kitchen staff

## Things I've messed with/done wrong because I've just started using this framework, and I don't really know what I'm doing

* I've installed the Ts.ED CLI tool as a project dependency, instead of a global dependency, as I don't really want it installed globally yet
* I forgot to use the `--dev` flag when doing the above, so the dependencies aren't under `devDependencies` in `package.json` (oops)
* I symlinked the `tsed` CLI tool into `./bin`, because I don't really want to install it globally yet

## Build setup

> **Important!** Ts.ED requires Node >= 10, Express >= 4 and TypeScript >= 3.

```batch
# install dependencies
$ yarn install

# serve
$ yarn start

# build for production
$ yarn build
$ yarn start:prod
```
